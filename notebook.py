import os
import argparse

from general import (
    DataReader, CRYPT_MODES,
    Analyzer,
)


class NoteBook(DataReader):
    """Класс шифрования / дешифровки с применением одноразового блокнота"""

    def __init__(self):
        super(NoteBook, self).__init__()

    def generate_one_time_pad(self, path, size=10240):
        self.save_data_to_file(
            data=os.urandom(size),
            file=path, mode="wb")

    def decrypt(self, cipher, notebook):
        return self.encrypt(cipher, notebook)

    def encrypt(self, plain, notebook):
        result = list()
        for index, item in enumerate(plain):
            result.append(item ^ notebook[index % len(notebook)])
        return bytes(result)

    def attack(self, plain, cipher):
        analyzer = Analyzer()
        plain_statistics = analyzer.get_code_statistics(plain)
        cipher_statistics = analyzer.get_code_statistics(cipher)
        analyzer.draw_graphic([plain_statistics, cipher_statistics])


def main(args):
    nb = NoteBook()
    result = None
    foo = None
    if args.mode == CRYPT_MODES["attack"]:
        nb.attack(
            nb.read_data_from_file(file=args.source, mode="rb"),
            nb.read_data_from_file(file=args.dest, mode="rb"),
        )
        print("Готово!")
        return
    if args.mode == CRYPT_MODES["generate"]:
        nb.generate_one_time_pad(args.source)
        print("Готово!")
        return
    if args.mode == CRYPT_MODES["encrypt"]:
        foo = nb.encrypt
    elif args.mode == CRYPT_MODES["decrypt"]:
        foo = nb.decrypt
    else:
        print("Неправильно указан режим работы (читай --help)")
        return
    if not args.notebook:
        print("Не указан путь до файла блокнота (читай --help)")
        return
    result = foo(
        nb.read_data_from_file(file=args.source, mode="rb"),
        nb.read_data_from_file(file=args.notebook, mode="rb")
    )
    nb.save_data_to_file(data=result, file=args.dest, mode="wb")
    print("Готово!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Шифрование с использованием одноразового блокнота')
    parser.add_argument(
        '-m',
        '--mode',
        required=True,
        action='store',
        dest='mode',
        help='Режим работы (encrypt / decrypt)')
    parser.add_argument(
        '--source',
        required=True,
        action='store',
        dest='source',
        help='Путь до исходного файла')
    parser.add_argument(
        '--dest',
        action='store',
        dest='dest',
        help='Путь до файла с результатом')
    parser.add_argument(
        '--notebook',
        action='store',
        dest='notebook',
        help='Путь до файла одноразового блокнота')
    args = parser.parse_args()
    main(args)
