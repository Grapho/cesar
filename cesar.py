import argparse

from general import (
    SYMBOLS_COUNT,
    CRYPT_MODES,
    DataReader,
    Analyzer,
    SYMBOLS,
)


class Cesar(DataReader):
    def __init__(self):
        super().__init__()

    def decrypt(self, data, shift):
        return self.encrypt(data=data, shift=-shift)

    def encrypt(self, data, shift):
        result = str()
        for letter in data:
            index_current = SYMBOLS.find(letter)
            index_new = (index_current + shift) % SYMBOLS_COUNT
            result += SYMBOLS[index_new]
        return result

    def attack(self, cipher, plain):
        analyzer = Analyzer()
        cipher_statistics = analyzer.get_letter_statistics(cipher)
        plain_statistics = analyzer.get_letter_statistics(plain)
        analyzer.draw_graphic([cipher_statistics, plain_statistics])


def main(args):
    cesar = Cesar()
    data = cesar.read_data_from_file(file=args.source)
    result = None
    foo = None
    if args.mode == CRYPT_MODES["encrypt"]:
        foo = cesar.encrypt
    elif args.mode == CRYPT_MODES["decrypt"]:
        foo = cesar.decrypt
    elif args.mode == CRYPT_MODES["attack"]:
        cesar.attack(
            cipher=data,
            plain=cesar.read_data_from_file(file=args.dest)
        )
        print("Готово!")
        return
    else:
        print("Неправильно указан режим работы (читай --help)")
        return
    if not args.shift:
        print("Не указан параметр --shift (сдвиг) (читай --help)")
        return
    if not args.shift.isnumeric():
        print("Неправильно указано число сдвига")
    else:
        args.shift = int(args.shift)

    result = foo(data=data, shift=args.shift)
    cesar.save_data_to_file(data=result, file=args.dest)
    print("Готово!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Шифр цезаря')
    parser.add_argument(
        '-s',
        '--shift',
        action='store',
        dest='shift',
        help='Число сдвига')
    parser.add_argument(
        '-m',
        '--mode',
        required=True,
        action='store',
        dest='mode',
        help='Режим работы (encrypt / decrypt)')
    parser.add_argument(
        '--source',
        required=True,
        action='store',
        dest='source',
        help='Путь до исходного файла')
    parser.add_argument(
        '--dest',
        required=True,
        action='store',
        dest='dest',
        help='Путь до файла с результатом')
    args = parser.parse_args()
    main(args)
