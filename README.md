# Основы криптографии

## Шифр цезаря

<a href="https://ru.wikipedia.org/wiki/Шифр_Цезаря">Официальная статья на вики</a>

```shell
  -h, --help                show this help message and exit
  -s SHIFT, --shift SHIFT   Число сдвига
  -m MODE, --mode MODE      Режим работы (encrypt / decrypt)
  --source SOURCE           Путь до исходного файла
  --dest DEST               Путь до файла с результатом
  ```

### Запуск

1. Зашифровать

```shell
  python cesar.py -m encrypt -s 5 --source test.txt --dest decrypted.txt
  ```

1. Расшифровать

```shell
  python cesar.py -m decrypt -s 5 --source encrypted.txt --dest decrypted.txt
  ```

## Шифр перестановок (вертикальный)

<a href="https://ru.wikipedia.org/wiki/Перестановочный_шифр">Официальная статья на вики</a>

```shell
  -h, --help            show this help message and exit
  -m MODE, --mode MODE  Режим работы (encrypt / decrypt)
  --source SOURCE       Путь до исходного файла
  --dest DEST           Путь до файла с результатом
  -k KEY, --key KEY     Ключ
  ```

### Запуск

1. Зашифровать

```shell
  python vertical.py -m encrypt -k GERMAN --source tmp/test.txt --dest tmp/encrypted.txt
  ```

1. Расшифровать

```shell
  python vertical.py -m decrypt -k GERMAN --source tmp/encrypted.txt --dest tmp/decrypted.txt
  ```

## Криптоанализ

### Запуск

```shell
  python analyzer.py --source tmp/test.txt --dest tmp/encrypted.txt -m cesar
  ```
