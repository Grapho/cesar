import argparse
import itertools

from general import (
    DataReader, timer,
    CRYPT_MODES
)


class Vertical(DataReader):
    def __init__(self):
        super().__init__()

    def transform(self, key):
        return [i[0] for i in sorted(enumerate(key), key=lambda x: x[1])]

    def decrypt(self, data, key):
        result = str()
        for k in range(len(data) // len(key)):
            for i in range(len(key)):
                index = self.transform(key).index(i)
                result += data[index * len(data) // len(key) + k]
        return result

    def encrypt(self, data, key):
        result = str()
        free_space = ((0 - len(data) % len(key)) % len(key))
        data = data + 'X' * free_space
        for i in self.transform(key):
            for k in range(len(data)):
                if k % len(key) == i:
                    result += data[k]
        return result

    @timer
    def attack(self, cipher, plain):
        print("Выполняю криптоанализ...")
        for key_len in range(1, len(cipher) // 2 + 1):
            for perm in itertools.permutations(set(cipher[:key_len])):
                key = ''.join(perm)
                decrypted = self.decrypt(cipher, key)
                if plain not in decrypted:
                    continue
                width = len(key)
                height = len(cipher) // width
                unique_letters = len(set(key))
                print(f'Длина ключа: {unique_letters}')
                print(f'Предполагаемый ключ: [{key}]')
                print(f'размерность таблицы: {width}x{height}')
                return
        print('Подходящий ключ не найден')


def main(args):
    vertical = Vertical()
    data = vertical.read_data_from_file(file=args.source)
    result = None
    foo = None
    if args.mode == CRYPT_MODES["encrypt"]:
        foo = vertical.encrypt
    elif args.mode == CRYPT_MODES["decrypt"]:
        foo = vertical.decrypt
    elif args.mode == CRYPT_MODES["attack"]:
        vertical.attack(
            cipher=vertical.read_data_from_file(file=args.dest),
            plain=data,
        )
        print("Готово!")
        return
    else:
        print("Неправильно указан режим работы (читай --help)")
        return
    if not args.key:
        print("Не указан ключ (читай --help)")
        return
    result = foo(data=data, key=args.key)
    vertical.save_data_to_file(data=result, file=args.dest)
    print("Готово!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Шифр вертикальной перестановки')
    parser.add_argument(
        '-m',
        '--mode',
        required=True,
        action='store',
        dest='mode',
        help='Режим работы (encrypt / decrypt)')
    parser.add_argument(
        '--source',
        required=True,
        action='store',
        dest='source',
        help='Путь до исходного файла')
    parser.add_argument(
        '--dest',
        required=True,
        action='store',
        dest='dest',
        help='Путь до файла с результатом')
    parser.add_argument(
        '-k',
        '--key',
        action='store',
        dest='key',
        help='Ключ')
    args = parser.parse_args()
    main(args)
